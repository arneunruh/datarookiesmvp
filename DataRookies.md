1. Login
2. Redux with Kepler and React-Admin
3. Isochrone
4. Login to get Alerts from different gateways
   1. SMS
   2. Whatsapp
   3. Social Media
   4. Signal
   5. Any Messenger
5. Location based Alert


## Lists of WMS, WFS
https://external.ogc.org/twiki_public/MetOceanDWG/MetocWMS_Servers

## Ogr2Ogr via GeoJson to PostGIS

ogr2ogr -f "PostgreSQL" PG:"host=db dbname=gis user=root password=toot" "/root/geoJson/1_deutschland/1_sehr_hoch.geo.json" -nln land
ogr2ogr -f "PostgreSQL" PG:"host=db dbname=gis user=root password=toot" "/root/geoJson/2_bundeslaender/1_sehr_hoch.geo.json" -nln bundeslaender
ogr2ogr -f "PostgreSQL" PG:"host=db dbname=gis user=root password=toot" "/root/geoJson/3_regierungsbezirke/1_sehr_hoch.geo.json" -nln regierungsbezirke
ogr2ogr -f "PostgreSQL" PG:"host=db dbname=gis user=root password=toot" "/root/geoJson/4_kreise/1_sehr_hoch.geo.json" -nln landkreise

## Build GDACS Content with Strapi

Strapi Admin
arne.unruh@bwi.de
+Pm8VnUAPG6PNG)


/datarookiesmvp

docker-compose up 

root / toot

localhost:8883 für Strapi
localhost:8882 für Hasura


cd /app

yarn oder npm install

yarn start

## Dump Databases

go into postgis container

cd /backup
pg_dumpall -U root > dump_all.tar
