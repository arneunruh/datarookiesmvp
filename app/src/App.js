import React, { useState, useEffect } from "react";
import {
  Admin,
  EditGuesser,
  ListGuesser,
  Loading,
  Resource,
  ShowGuesser,
} from "react-admin";

import { MyLayout } from "./components/Layout";
import { Dashboard } from "./components/Dashboard";
import { EventMapList } from "./components/EventMapList";
import { SocialMapList } from "./components/SocialMapList";

import { customRoutes } from "./routes/custom.routes";
import { buildHasuraDataProvider } from "./services/data.provider/apollo.client";
import { createBrowserHistory as createHistory } from "history";

const history = createHistory();

function App() {
  const [dataProvider, setDataProvider] = useState(null);

  useEffect(() => {
    const buildProvider = async () => {
      const dataProvider = await buildHasuraDataProvider();
      setDataProvider(() => dataProvider);
    };
    buildProvider();
  }, []);

  if (dataProvider === null) {
    return <Loading />;
  }

  return (
    <Admin
      disableTelemetry
      customRoutes={customRoutes}
      history={history}
      dashboard={Dashboard}
      dataProvider={dataProvider}
      layout={MyLayout}
      title={"Natural Disaster Protection"}
    >
      <Resource
        name="Events"
        options={{ label: "Warnungen" }}
        list={EventMapList}
        edit={EditGuesser}
        show={ShowGuesser}
      />

      <Resource
        name="tweet_forecast_lonlat"
        options={{ label: "Social Media Prediction" }}
        list={SocialMapList}
        edit={EditGuesser}
        show={ShowGuesser}
      />
    </Admin>
  );
}

export default App;
