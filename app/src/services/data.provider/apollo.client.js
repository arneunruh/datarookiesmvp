import {
  ApolloClient,
  InMemoryCache,
} from '@apollo/client';

import buildHasuraProvider from 'ra-data-hasura';

//write to env file
const endpoint = 'http://localhost:8882/v1/graphql';

const createApolloClient = async () => {
  return new ApolloClient({
    uri: endpoint,
    headers: {
      //write to env file
      'x-hasura-admin-secret': 'secretkey',
    },
    cache: new InMemoryCache(),
  });
};

const buildHasuraDataProvider = async () => {
  const apolloClient = await createApolloClient();

  const dataProvider = await buildHasuraProvider(
    {
      client: apolloClient,
    }
  );

  return dataProvider;
};

export { buildHasuraDataProvider };
