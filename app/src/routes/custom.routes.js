import React from 'react';
import { Route } from 'react-router-dom';
import { Search } from '../components/Search';
import { Alert } from '../components/Alert';

//https://stackoverflow.com/questions/51525116/custom-routes-in-react-admin

const paths = {
    Search: '/search',
    Alert: '/alert',
};

export const customRoutes = [
    <Route exact path={paths.Search} component={Search} />,
    <Route exact path={paths.Alert} component={Alert} />
];