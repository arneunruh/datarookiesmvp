import React, { useState } from "react";
import { useListContext } from "react-admin";
import { useLocation } from "react-router";
import { Drawer, Box, IconButton } from "@material-ui/core";

import { makeStyles } from "@material-ui/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import {
  MapContainer,
  TileLayer,
  LayersControl,
  WMSTileLayer,
  Marker,
  Popup,
  Polyline,
} from "react-leaflet";
import MarkerClusterGroup from "react-leaflet-markercluster";
import DefaultPopupContent from "./DefaultPopupContent";
import QueryStringUpdater from "./QueryStringUpdater";
import CloseIcon from "@material-ui/icons/Close";

import L from "leaflet";

import icon from "leaflet/dist/images/marker-icon.png";
import iconShadow from "leaflet/dist/images/marker-shadow.png";

let DefaultIcon = L.icon({
  iconUrl: icon,
  shadowUrl: iconShadow,
});

L.Marker.prototype.options.icon = DefaultIcon;

const useStyles = makeStyles(() => ({
  loading: {
    zIndex: 1000,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  closeButton: {
    position: "absolute",
    zIndex: 1400,
    top: 0,
    right: 0,
  },
}));

/**
 * OpenWeatherMap
 * Stolen Api Key from random github project- must register in production
 */

//API KEY
const OWM_KEY = "876f71af64d39434b74d05b31a77fc42";

//Open Street Maps
const osm_attribution =
  '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors';
const osm_source = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";

//OpenWeather
const owm_attribution =
  '<a href="https://openweathermap.org/">OpenWeatherMap</a>';
const owm_url = {
  a: "https://tile.openweathermap.org/map/",
  b: `/{z}/{x}/{y}.png?appid=${OWM_KEY}`,
};

//OWM Layer values
const layers = {
  clouds: "clouds_new",
  precip: "precipitation_new",
  pressure: "pressure_new",
  wind: "wind_new",
  temp: "temp_new",
};

//GEOServer

const MapList = ({
  latitude,
  longitude,
  label,
  description,
  popupContent,
  height,
  center,
  zoom,
  groupClusters,
  boundToMarkers,
  connectMarkers,
  ...otherProps
}) => {
  const { ids, data, basePath, loading } = useListContext();
  const [drawerRecord, setDrawerRecord] = useState(null);
  const [map, setMap] = useState(null);
  const classes = useStyles();

  // Get the zoom and center from query string, if available
  const query = new URLSearchParams(useLocation().search);
  center =
    query.has("lat") && query.has("lng")
      ? [query.get("lat"), query.get("lng")]
      : center;
  zoom = query.has("zoom") ? query.get("zoom") : zoom;

  let previousRecord;

  const records = ids
    .map((id) => ({
      ...data[id],
      latitude: latitude && latitude(data[id]),
      longitude: longitude && longitude(data[id]),
      label: label && label(data[id]),
      description: description && description(data[id]),
    }))
    .filter((record) => record.latitude && record.longitude);

  const bounds =
    boundToMarkers && records.length > 0
      ? records.map((record) => [record.latitude, record.longitude])
      : undefined;

  // Do not display anything if the bounds are not ready, otherwise the MapContainer will not be initialized correctly
  if (boundToMarkers && !bounds) return null;

  const markers = records.map((record, i) => {
    const marker = (
      <React.Fragment key={i}>
        <Marker position={[record.latitude, record.longitude]}>
          <Popup>
            {React.createElement(popupContent, { record, basePath })}
          </Popup>
        </Marker>
        {connectMarkers && previousRecord && (
          <Polyline
            positions={[
              [previousRecord.latitude, previousRecord.longitude],
              [record.latitude, record.longitude],
            ]}
          />
        )}
      </React.Fragment>
    );

    // Save record so that we can trace lines
    previousRecord = record;

    return marker;
  });

  return (
    <div>
      <MapContainer
        style={{ height }}
        center={!boundToMarkers ? center : undefined}
        zoom={!boundToMarkers ? zoom : undefined}
        bounds={bounds}
        whenCreated={setMap}
        {...otherProps}
      >
        <LayersControl position="topright">
          <LayersControl.BaseLayer checked name="Open Street Maps">
            <TileLayer attribution={osm_attribution} url={osm_source} />
          </LayersControl.BaseLayer>
          <LayersControl.Overlay name="Clouds">
            <TileLayer
              attribution={owm_attribution}
              url={owm_url.a + layers.clouds + owm_url.b}
            />
          </LayersControl.Overlay>
          <LayersControl.Overlay name="Pricipitation">
            <TileLayer
              attribution={owm_attribution}
              url={owm_url.a + layers.precip + owm_url.b}
            />
          </LayersControl.Overlay>
          <LayersControl.Overlay name="Pressure">
            <TileLayer
              attribution={owm_attribution}
              url={owm_url.a + layers.pressure + owm_url.b}
            />
          </LayersControl.Overlay>
          <LayersControl.Overlay name="Wind">
            <TileLayer
              attribution={owm_attribution}
              url={owm_url.a + layers.wind + owm_url.b}
            />
          </LayersControl.Overlay>
          <LayersControl.Overlay name="Temp">
            <TileLayer
              attribution={owm_attribution}
              url={owm_url.a + layers.temp + owm_url.b}
            />
          </LayersControl.Overlay>
          <LayersControl.Overlay name="Landkreise">
            <WMSTileLayer
              layers={"DataRookies:landkreise"}
              url="http://localhost:8881/geoserver/DataRookies/wms?service=WMS"
              version="1.3.0"
              format="image/png"
              transparent="true"
            />
          </LayersControl.Overlay>
          {loading && (
            <Box alignItems="center" className={classes.loading}>
              <CircularProgress size={60} thickness={6} />
            </Box>
          )}
          {groupClusters ? (
            <MarkerClusterGroup showCoverageOnHover={false}>
              {markers}
            </MarkerClusterGroup>
          ) : (
            markers
          )}
          <QueryStringUpdater />
          <Drawer
            anchor="bottom"
            open={!!drawerRecord}
            onClose={() => setDrawerRecord(null)}
          >
            <Box p={1} position="relative">
              <IconButton
                onClick={() => setDrawerRecord(null)}
                className={classes.closeButton}
              >
                <CloseIcon />
              </IconButton>
              {drawerRecord &&
                React.createElement(popupContent, {
                  record: drawerRecord,
                  basePath,
                })}
            </Box>
          </Drawer>
        </LayersControl>
      </MapContainer>
    </div>
  );
};

MapList.defaultProps = {
  height: 500,
  center: [50.73438, 7.09549],
  zoom: 9,
  groupClusters: true,
  connectMarkers: false,
  scrollWheelZoom: false,
  popupContent: DefaultPopupContent,
};

export default MapList;
