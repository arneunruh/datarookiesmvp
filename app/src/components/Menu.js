import * as React from 'react';
import { useSelector } from 'react-redux';
import { MenuItemLink, getResources } from 'react-admin';
import DefaultIcon from '@material-ui/icons/ViewList';
import SearchIcon from '@material-ui/icons/Search';
import DashboardIcon from '@material-ui/icons/Dashboard';


export const Menu = () => {

    const resources = useSelector(getResources);

    const isOpen = useSelector((state) => state.admin.ui.sidebarOpen);

    return (
        <>
            <MenuItemLink to="/" primaryText={isOpen ? 'Dashboard' : ''} leftIcon={<DashboardIcon />} />
            {resources.map(resource => (
                <MenuItemLink
                    key={resource.name}
                    to={`/${resource.name}`}
                    primaryText={
                        isOpen ?
                        (resource.options && resource.options.label) ||
                        resource.name
                        : ''
                    }
                    leftIcon={
                        resource.icon ? <resource.icon /> : <DefaultIcon />
                    }
                />
            ))}
            <MenuItemLink to="/search" primaryText={isOpen ? 'Suche' : ''} leftIcon={<SearchIcon />} />
            {
                /**
                 * 
                 * <MenuItemLink to="/map" primaryText={isOpen ? 'Karte' : ''} leftIcon={<SearchIcon />} />
                 * <MenuItemLink to="/alert" primaryText={isOpen ? 'Alarm' : ''} leftIcon={<SearchIcon />} />
                 * 
                 */
            }
            
        </>
    );



}

