import {
  List,
  Datagrid,
  TextField,
  NumberField,
  DateField,
  BooleanField,
} from "react-admin";
import { MapList } from "./geo-component";

export const SocialMapList = (props) => (
  <div>
    <List pagination={false} perPage={1000} {...props}>
      <MapList
        latitude={(record) => record.lat}
        longitude={(record) => record.lon}
        label={(record) => record.place}
        description={(record) => record.text}
      />
    </List>
    <List pagination={false} perPage={1000} {...props}>
      <Datagrid rowClick="edit">
        <DateField source="date" />
        <TextField source="id" />
        <NumberField source="lat" />
        <NumberField source="lon" />
        <TextField source="natoCode" />
        <TextField source="place" />
        <TextField source="text" />
      </Datagrid>
    </List>
  </div>
);
