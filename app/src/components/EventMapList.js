import { List, Datagrid, TextField , NumberField, DateField, BooleanField} from "react-admin";
import { MapList } from "./geo-component";

export const EventMapList = (props) => (
  <div>
    <List pagination={false} perPage={1000} {...props}>
      <MapList
        latitude={(record) => record.latitude}
        longitude={(record) => record.longitude}
        label={(record) => record.title}
        description={(record) => record.description}
      />
    </List>
    <List pagination={false} perPage={1000} {...props}>
      <Datagrid>
        <TextField source="alertLevel" />
        <NumberField source="alertScore" />
        <TextField source="title" />
        <TextField source="description" />
        <TextField source="district" />
        <TextField source="state" />
        <TextField source="durationInWeek" />
        <TextField source="eventType" />
        <DateField source="fromDate" />
        <DateField source="toDate" />
        <BooleanField source="isCurrent" />
        <TextField source="subject" />
        <BooleanField source="temporary" />
        <NumberField source="version" />
        <NumberField source="vulnerable" />
        <NumberField source="year" />
      </Datagrid>
    </List>
  </div>
);
